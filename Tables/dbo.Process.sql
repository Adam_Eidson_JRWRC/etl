CREATE TABLE [dbo].[Process]
(
[ProcessId] [int] NOT NULL IDENTITY(1, 1),
[processName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[filePath] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
