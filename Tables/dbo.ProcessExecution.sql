CREATE TABLE [dbo].[ProcessExecution]
(
[ProcessExecutionid] [int] NOT NULL IDENTITY(1, 1),
[processId] [int] NOT NULL,
[messageLevelId] [int] NOT NULL,
[message] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
