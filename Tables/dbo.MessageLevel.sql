CREATE TABLE [dbo].[MessageLevel]
(
[MessageLevelId] [int] NOT NULL IDENTITY(1, 1),
[messageLevelName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[messageLevelDescription] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
