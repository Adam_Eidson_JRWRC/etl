CREATE TABLE [dbo].[log]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[log_level] [int] NULL,
[log_levelname] [char] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[log] [char] (2048) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[created_at] [datetime2] NOT NULL,
[created_by] [char] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
