SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC ETL.dbo.[EmailLastHourErrors]
CREATE PROCEDURE [dbo].[EmailLastHourErrors] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @body_content nvarchar(max);
SET @body_content = N'


<table >
  <th>
    <tr>
      <th>CreatedBy</th>
      <th>Log</th>
      <th>Count</th>
    </tr>
  </th>
  <td>' +
CAST(
        (
	Select created_by as 'td','', log as 'td','',
	count(*) as 'td'
	From ETL.dbo.log  
	where log_levelname = 'ERROR'
	and created_at >= dateadd(hour, -1,getdate())
	and created_by not in ('CeleryAppEventlet_Logger',
	'PArseSubDocPDF_ParsePDF_LOGGER',
	'PArseSubDocPDF_PrepareSubDocInsertPayload_LOGGER',
	'SubDoc_LOGGER'
	)
	group by created_by,
	log
	order by created_by, log desc
        FOR XML PATH('tr'), TYPE   
        ) AS nvarchar(max)
    ) +
  N'</td>
</table>';


--only send email if there are errors to report.
if (Select
	count(*)  as total
	From ETL.dbo.log  
	where log_levelname = 'ERROR'
	and created_at >= dateadd(hour, -1,getdate())
	and created_by not in ('CeleryAppEventlet_Logger',
	'PArseSubDocPDF_ParsePDF_LOGGER',
	'PArseSubDocPDF_PrepareSubDocInsertPayload_LOGGER',
	'SubDoc_LOGGER'
	)) > 0
	BEGIN
	
EXEC msdb.dbo.sp_send_dbmail  
    @profile_name = 'ETLAPI_EMailProfile',  
    @recipients = 'aeidson@jrwrc.com',
    @body = @body_content,  
	@body_format='HTML',
    @subject = 'List of Errors in Past Hour' 
	END

END
GO
