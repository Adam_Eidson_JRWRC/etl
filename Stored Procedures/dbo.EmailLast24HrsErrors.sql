SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC ETL.dbo.EmailLast24HrsErrors
CREATE PROCEDURE [dbo].[EmailLast24HrsErrors] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @body_content nvarchar(max);
SET @body_content = N'


<table >
  <th>
    <tr>
      <th>CreatedBy</th>
      <th>Log</th>
      <th>Count</th>
    </tr>
  </th>
  <td>' +
CAST(
        (
	Select created_by as 'td','', log as 'td','',
	count(*) as 'td'
	From ETL.dbo.log  
	where log_levelname = 'ERROR'
	and created_at >= dateadd(day, -1,getdate())
	group by created_by,
	log
	order by created_by, log desc
        FOR XML PATH('tr'), TYPE   
        ) AS nvarchar(max)
    ) +
  N'</td>
</table>';
EXEC msdb.dbo.sp_send_dbmail  
    @profile_name = 'ETLAPI_EMailProfile',  
    @recipients = 'aeidson@jrwrc.com;lee.hultman@theespresseo.com;LSwingrover@exchangeright.com;bgolly@jrwrc.com',  
    @body = @body_content,  
	@body_format='HTML',
    @subject = 'List of Errors in Past 24 hrs' 
END
GO
