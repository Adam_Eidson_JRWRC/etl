SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Last24HrsErrors] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select created_by, log,
	count(*) as NumberOfErrors
	From ETL.dbo.log  
	where log_levelname = 'ERROR'
	and created_at >= dateadd(day, -1,getdate())
	group by created_by,
	log
	order by created_by, log desc
END
GO
